﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    
    public class datos
    {
        private SqlConnection con;
        public DataTable Tabla_Datos;
        public datos()
        {
            String connectString = "Data Source=tiendasitios.mssql.somee.com;Initial Catalog=tiendaSitios;User ID= valdo50933_SQLLogin_1; Password=wab6qe25r8";
            this.con = new SqlConnection(connectString);
            this.con.Open();
        }

        public void executeQueryDT(String select)
        {
            try
            {
                Tabla_Datos = new DataTable();
                SqlCommand com = new SqlCommand(select, this.con);
                com.ExecuteNonQuery();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(com);
                sqlDataAdapter.Fill(Tabla_Datos);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
            
        }

        public void executeQuerySD(string query)
        {
            try
            {
                SqlCommand com = new SqlCommand(query, this.con);
                com.ExecuteNonQuery();       
                this.con.Close();
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consutarProductos()
        {
            try
            {
                String select = "SELECT idProducto, nombre, costo, urlImagen from productos";
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }


        }

        public void consutarMarcas()
        {
            try
            {
                String select = "SELECT idMarca, nombre, urlImagen from Marcas";
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consutarProductoPorId(int id)
        {
            try
            {
                String select = "SELECT idProducto, nombre, costo, urlImagen, descripcion from productos where idProducto = "+id;
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consutarProductoPorIdMarca(int id)
        {
            try
            {
                String select = "SELECT idProducto, nombre, costo, urlImagen from productos where idMarca = " + id;
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void ingresarComentarioProducto(int id, string nombre, string comentario)
        {
             try
            {
                String select = "Insert into comentarioProductos (idProducto, nombre, comentario) values ("+ id +","+"'"+nombre+"','"+comentario+"')";
                this.executeQuerySD(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void ingresarComentarioPorPersonas(int id, string nombre, string comentario)
        {
            try
            {
                String select = "Insert into comentariosProducto (idProducto, nombre, comentario) values ("+id+","+"'"+nombre+"','"+comentario+"')";
                this.executeQuerySD(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consultarComentariosPorProducto(int id){
             try
            {
                String select = "SELECT nombre, comentario from comentarioProductos where idProducto =" + id;
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consultarComentariosPorPersona(int id){
             try
            {
                String select = "SELECT nombre, comentario from comentariosEstudiantes where idEstudiante =" + id;
                this.executeQuerySD(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consutarNombres()
        {
            try
            {
                String select = "SELECT nombre from Estudiantes";
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }

        public void consutarNombrePorId()
        {
            try
            {
                String select = "SELECT nombre from Estudiantes";
                this.executeQueryDT(select);
            }
            catch (SqlException sql)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw sql;
            }
            catch (Exception ex)
            {
                if (this.con.State == ConnectionState.Open)
                {
                    this.con.Close();
                }
                throw ex;
            }
        }
    }
}



