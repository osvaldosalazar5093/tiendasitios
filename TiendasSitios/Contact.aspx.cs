﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TiendasSitios
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BLL.productos imarcas = new BLL.productos();
            DataTable dt = imarcas.consultarMarcas();
            StringBuilder misMarcas = new StringBuilder();
            misMarcas.Append("<div class=\"row d-flex justify-content-center\" > \n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                misMarcas.Append("<div class='card col-md-6 d-flex justify-content-center'>");
                misMarcas.Append("<form method=\"POST\">");
                misMarcas.Append("<img src = \" recursos/" + dt.Rows[i][2] + "\" alt=\"Imagen producto\" style=\"width:60%; transition: 0.3s; padding 2px 16px \"> \n");
                misMarcas.Append("<div class=\"container\"> \n");
                misMarcas.Append("<h4> <b>" + dt.Rows[i][1] + "</b> </h4> \n");
                misMarcas.Append("</div> \n");
                misMarcas.Append("</form>");
                misMarcas.Append("</div> \n");

            }
            misMarcas.Append("</div> \n");
            this.marcas.InnerHtml = misMarcas.ToString();
        }
    }
}  
