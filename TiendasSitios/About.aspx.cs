﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TiendasSitios
{
    public partial class About : Page
    {
        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString.Get("m") != null)
            { 
                int num = Int32.Parse(Request.QueryString.Get("m").ToString());

                BLL.productos iProducto = new BLL.productos();
                DataTable dt = iProducto.buscarProductoPorID(num);

                StringBuilder misProductos = new StringBuilder();
                misProductos.Append("<div class=\"row d-flex justify-content-center\" > \n");
                misProductos.Append("<div class=\"col-md-12 d-flex justify-content-center \">");
                misProductos.Append("<img src = \" recursos/" + dt.Rows[0][3] + "\" alt=\"Imagen producto\" style=\"width:60%; transition: 0.3s; padding 2px 16px \"> \n");
                misProductos.Append("</div>");
                misProductos.Append("<div class=\"col-md-12 d-flex justify-content-center \">");
                misProductos.Append("<h2>"+ dt.Rows[0][1] + "</h3>");
                misProductos.Append("</div>");
                misProductos.Append("<div class=\"col-md-12 d-flex justify-content-center \">");
                misProductos.Append("<h3>" + dt.Rows[0][4] + "</h3>");
                misProductos.Append("</div>");
                misProductos.Append("<div class=\"col-md-12 d-flex justify-content-center \">");
                misProductos.Append("<h3>" + dt.Rows[0][2] + "</h3>");
                misProductos.Append("</div>");
                misProductos.Append("</div>");

                this.product.InnerHtml=misProductos.ToString();
                id = (int)dt.Rows[0][0];
                DataTable dtComentarios = iProducto.buscarComentarios(id);
                gvComentarios.DataSource=dtComentarios;
                gvComentarios.DataBind();
                

            }
            else
            {
                //Poner mensaje
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            BLL.productos iProducto = new BLL.productos();
            iProducto.ingresarComentarios(id, this.txtNombre.Text, this.txtComentario.Text);   
        }
    }
}