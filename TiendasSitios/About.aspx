﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="TiendasSitios.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="product"></div>

    <asp:GridView ID="gvComentarios" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="830px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#E9E7E2" />
    </asp:GridView>


    <div class="row">
        <div class="col-md-4">
            <label>Nombre:</label><br />
            <asp:TextBox runat="server" ID="txtNombre"/>
        </div>
        <div class="col-md-12">
            <label>Comentario:</label><br/>
            <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:Button Text="Comentar" ID="btnAgregar" runat="server" OnClick="btnAgregar_Click" />
        </div>
    </div>
    
</asp:Content>
